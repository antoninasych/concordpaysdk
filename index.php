<?php
require_once("ConcordPaySDK.php");
if (file_exists('./env.php')) {
    include './env.php'; // define  const PRIVATE_KEY, MERCHANT_ID etc in env.php
}



$sdk = new ConcordPaySDK(PRIVATE_KEY);

#----Оплата на платежной странице процессинга----------------------------------------------------------

$params = [
    "operation" => "Purchase",
    "merchant_id" => MERCHANT_ID,
    "amount" => 0.1,
    "order_id" => 60,
    "currency_iso" => "UAH",
    "description" => "test",
    "approve_url" => "http://sdk.loc/reciver.php",
    "decline_url" => "http://ecom.local/decline",
    "callback_url" => "http://sdk.loc/reciver.php",
    "cancel_url" => "http://ecom.local/cancel"
];
 echo $sdk->purchase($params);


#--------------------RecPayment------------------------------------------


//$params = [
//    "operation" => "RecPayment",
//    "merchant_id" => MERCHANT_ID,
//    "amount" => 0.3,
//    "order_id" => 90,
//    "currency_iso" => "UAH",
//    "recurring_token"=> TOKEN,
//    "description" => "Рекурентный платеж тестирование SDK",
//    "approve_url" => "http://sdk.loc/reciver.php",
//    "decline_url" => "http://ecom.local/decline",
//    "callback_url" => "http://sdk.loc/reciver.php",
//    "cancel_url" => "http://ecom.local/cancel",
//    "add_params"=>4
//];
//
//echo  $sdk->recPayment($params);


#------Verify--------------------------------------------------------

//
//$params1 = [
//    "operation" => "RecPayment",
//    "merchant_id" => MERCHANT_ID,
//    "amount" => 0.3,
//    "order_id" => "WgtRVIaKHy",
//    "currency_iso" => "UAH",
//    "recurring_token"=> PRIVATE_KEY,
//    "description" => "Verify",
//    "approve_url" => "http://sdk.loc/reciver.php",
//    "decline_url" => "http://ecom.local/decline",
//    "callback_url" => "http://sdk.loc/reciver.php",
//    "cancel_url" => "http://ecom.local/cancel",
//    "add_params"=>4
//];
//
//  print_r( $sdk->verify($params1));


#--------Reversal------------------------------------------------------


//$reversal = [
//    "merchant_id" => MERCHANT_ID,
//    "order_id" => 60
//];
//$data =  $sdk->reversal($reversal);
//print_r($data);


#----------Complete----------------------------------------------------
//
//$complete= [
//    "operation" => "Complete",
//    "merchant_id" => MERCHANT_ID,
//    "amount" => 0.1,
//    "order_id" => 60
//];
//$data =  $sdk->complete($complete);
//print_r($data);

#----------Check----------------------------------------------------

//$check= [
//
//    "merchant_id" => MERCHANT_ID,
//    "order_id" => 60
//];
//$data =  $sdk->check($check);
//print_r($data);
//Пример ответа:
//stdClass Object ( [code] => 0 [merchantAccount] => m_dobrodiy [orderReference] => 60 [amount] => 0.10 [currency] => UAH [phone] => [createdDate] => 2020-04-21 17:14:49 [cardPan] => 403021******9876 [cardType] => Visa [fee] => 0.00 [transactionId] => 28122001 [transactionStatus] => REFUNDED [reverseAmount] => 0.10 [reverseDate] => 2020-04-21 19:07:48 [reason] => ОПЕРАЦИЯ РАЗРЕШЕНА [reasonCode] => 1 )

#----------P2PCredit----------------------------------------------------


//$params = [
//    "operation" => "P2PCredit",
//    "merchant_id" => MERCHANT_ID,
//    "order_id" => 601,
//    "amount" => 1,
//    "card_number" => "5352772000231297",
//    "currency_iso" => "UAH"
//];
//
// $data =  $sdk->p2pCredit($params);

#----------GetBalance----------------------------------------------------

//$params = [
//    "operation" => "GetBalance",
//    "merchant_id" => MERCHANT_ID,
//    "date" => "2020-04-10 15:45:00"//YYYY-MM-DD HH:II:SS
//];
//
//print_r( $sdk->getBalance($params));
// print_r( $sdk->createSignature([$params['merchant_id'],$params['date']]));

#----------P2PDebit--------------------------------------------------------
//$params = [
//    "operation" => "P2PDebit",
//    "merchant_id" => MERCHANT_ID,
//    "amount" => 1,
//    "order_id" => 61,
//    "currency_iso" => "UAH",
//    "description" => "test",
//    "approve_url" => "http://sdk.loc/reciver.php",
//    "decline_url" => "http://ecom.local/decline",
//    "callback_url" => "http://sdk.loc/reciver.php",
//    "cancel_url" => "http://ecom.local/cancel",
//    "add_params"=>4
//];
// echo $sdk->p2pDebit($params);


#----------PurchaseOnMerchant--------------------------------------------------------
//$params = [
//    "operation" => "PurchaseOnMerchant",
//    "merchant_id" => MERCHANT_ID,
//    "amount" => 1,
//    "order_id" => 600,
//    "currency_iso" => "UAH",
//    "description" => "PurchaseOnMerchant test",
//    "add_params" => 4,
//    'card_num' => CARD_NUM,
//    'card_exp_month' => CARD_EXP_MONTH,
//    'card_exp_year' => CARD_EXP_YEAR,
//    'card_cvv' => CARD_CVV,
//    'card_holder' => 'Antonina Sych',
//    'payment_type' => 'Purchase',
//    'secure_type' => '3DS',
//    'callback_url' => 'http://sdk.loc/reciver.php'
//
//];
//echo $sdk->purchaseOnMerchant($params);

#----------Получение токена для Masterpass--------------------------------------------------------
//$params = [
//    "msisdn" => PHONE,
//    "client_id" => "1234567890765",
//     "merchant_id" => MERCHANT_ID
//];
// $token = $sdk->getMasterpassToken($params);
// print_r($token);
//
//#----------Making a payment through Masterpass (расскоментировать предыдущий блок для получения токена)--------------------------------------------------------
//$params = [
//    "operation" => "PurchaseMasterpass",
//    "merchant_id" => MERCHANT_ID,
//    "amount"=>1,
//    "order_id"=>23,
//    "currency_iso"=>"UAH",
//    "description"=>"test123",
//    "approve_url"=>"http://sdk.loc/reciver.php",
//    "decline_url"=>"http://sdk.loc/reciver.php",
//    "cancel_url"=>"http://sdk.loc/reciver.php",
//    "callback_url"=>"http://sdk.loc/reciver.php",
//    "add_params"=>[
//        "wallet"=>"masterpass",
//        "msisdn"=>PHONE,
//        "token"=>$token,
//        "card_name"=>"alias from server Master Pass",
//        "client_id"=>"Client Id",
//        "ret_ref_no"=>"Number unic from Masterpass",
//    ]
//];
// print_r( $sdk->purchaseMasterpass($params));

#----------3DS--------------------------------------------------------

//$params = [
//    "operation" => "Complete3DS",
//    "transaction_key" => "KEY",
//    "merchant_id" => MERCHANT_ID,
//    "d3ds_md" => D3DS_MD,
//    "d3ds_pares" => D3DS_PARES,
//];
//print_r( $sdk->confirm3DS($params));
